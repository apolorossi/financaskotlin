package com.apolorossi.financask.extension

import java.text.SimpleDateFormat
import java.util.*

fun String.limitTo(characters: Int) : String {
    if (this.length > characters){
        val firstCharacter = 0
        return "${this.substring(firstCharacter, characters)}..."
    } else {
        return this
    }
}

fun String.parseToCalendar() : Calendar {
    val brazilianFormat = SimpleDateFormat("dd/MM/yyyy")
    val dateParsed: Date = brazilianFormat.parse(this)
    val date = Calendar.getInstance()
    date.time = dateParsed

    return date
}