package com.apolorossi.financask.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import com.apolorossi.financask.R
import com.apolorossi.financask.model.Transaction
import com.apolorossi.financask.model.Type
import com.apolorossi.financask.ui.ResumeView
import com.apolorossi.financask.ui.adapter.ListTransactionsAdapter
import com.apolorossi.financask.ui.dialog.AddTransactionDialog
import com.apolorossi.financask.ui.dialog.UpdateTransactionDialog
import kotlinx.android.synthetic.main.activity_lista_transacoes.*

class ListaTransacoesActivity : AppCompatActivity() {

    private val transactionsList : MutableList<Transaction> = mutableListOf()

    /**
        Note: It's not possible initialize the 'activityView' here because the layout was not drawn yet. Then only in 'onCreate()' method
        will be possible to initialize the property.

        There are some ways to solve this problem:

        1 - The Elvis Operator '?' is used to FORCE the property to accept the null initialization

            private var activityView : ViewGroup? = null

        2 - The late init allow you to initialize the var later, but it has some problems like the type var. And the fact that if there too many
            it's hard to know where it one is being initialized.

            private lateinit var activityView : ViewGroup

        3 - The lazy is smarter because it makes the initialization of var in the exact moment it has to be used. It's initialization method is made with the var declaration
            The only risk is that if another var try to be initialized with a lazy var in a point that the lazy cannot be initialized

            private val activityView by lazy {
                window.decorView
            }

     */
    private val activityView by lazy {
        window.decorView
    }

    private val activityViewGroup by lazy {
        activityView as ViewGroup
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_lista_transacoes)


        setupResume()

        setupList()

        setupFabClickListeners()
    }

    fun setupFabClickListeners() {
        //Object Expression - indica que queremos realizar a implementação de uma Interface
        lista_transacoes_adiciona_receita
                .setOnClickListener {
                    callTransactionDialog(Type.RECEITA)
                }

        lista_transacoes_adiciona_despesa
                .setOnClickListener {
                    callTransactionDialog(Type.DESPESA)
                }
    }

    fun addTransation(transaction: Transaction) {
        transactionsList.add(transaction)
        updateTransactions()
    }


    fun updateTransactions() {
        setupList()
        setupResume()
    }

    fun setupResume() {
        ResumeView(this, activityView , transactionsList).update()
    }

    fun setupList() {

        val transactionsListAdapter = ListTransactionsAdapter(transactionsList, this)
        with(lista_transacoes_listview) {
            adapter = transactionsListAdapter
            setOnItemClickListener { _, _, position, _ ->
                val transaction = transactionsList[position]
                callUpdateDialog(transaction, position)
            }
        }
    }

    fun callTransactionDialog(type: Type) {
        AddTransactionDialog(activityViewGroup, this)
                .setupTransationDialog(type) { createdTransaction -> // HighOrderFunction
                    addTransation(createdTransaction)
                    lista_transacoes_adiciona_menu.close(true)
                }
    }

    fun callUpdateDialog(transaction: Transaction, position: Int) {
        UpdateTransactionDialog(activityViewGroup, this)
                .show(transaction) { updatedTransaction ->
                    updateTransactionList(position, updatedTransaction)
                }

    }

    fun updateTransactionList(position: Int, transaction: Transaction) {
        transactionsList[position] = transaction // Atualiza um elemento específico da lista
        updateTransactions()
    }

}