package com.apolorossi.financask.ui.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.apolorossi.financask.R
import com.apolorossi.financask.extension.formatToBrazilian
import com.apolorossi.financask.extension.formatToBrazilianCurrency
import com.apolorossi.financask.extension.limitTo
import com.apolorossi.financask.model.Transaction
import com.apolorossi.financask.model.Type
import kotlinx.android.synthetic.main.transacao_item.view.*

class ListTransactionsAdapter(private val mTransactionList: List<Transaction>,
                              private val mContext: Context) : BaseAdapter() {

    private val CATEGORY_LIMIT = 14

    override fun getView(position: Int, view: View?, viewGroup: ViewGroup?): View {
        val viewCreated = LayoutInflater.from(mContext)
                .inflate(R.layout.transacao_item, viewGroup, false)

        val transaction = mTransactionList[position]

        addIcon(transaction, viewCreated)
        addValue(transaction, viewCreated)
        addCategory(viewCreated, transaction)
        addDate(viewCreated, transaction)

        return viewCreated
    }

    fun addDate(viewCreated: View, transaction: Transaction) {
        viewCreated.transacao_data.text =
                transaction.date.formatToBrazilian()
    }

    fun addCategory(viewCreated: View, transaction: Transaction) {
        viewCreated.transacao_categoria.text =
                transaction.category.limitTo(CATEGORY_LIMIT)
    }

    fun addIcon(transaction: Transaction, viewCreated: View) {

        viewCreated.transacao_icone
                .setBackgroundResource(getIconByType(transaction))
    }

    fun getIconByType(transaction: Transaction): Int {
        if (transaction.type == Type.RECEITA) {
            return R.drawable.icone_transacao_item_receita
        }

        return R.drawable.icone_transacao_item_despesa

    }

    fun addValue(transaction: Transaction, viewCreated: View) {
        val color: Int = colorByType(transaction.type)

        viewCreated.transacao_valor
                .setTextColor(color)
        viewCreated.transacao_valor.text =
                transaction.value.formatToBrazilianCurrency()
    }

    fun colorByType(type: Type): Int {
        if (type == Type.RECEITA) {
            return ContextCompat.getColor(mContext, R.color.receita)
        } else {
            return ContextCompat.getColor(mContext, R.color.despesa)
        }
    }

    override fun getItem(position: Int): Transaction {// By default return Any, but changed to the type of List, in this case, "String".
        return mTransactionList[position]
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return mTransactionList.size
    }
}