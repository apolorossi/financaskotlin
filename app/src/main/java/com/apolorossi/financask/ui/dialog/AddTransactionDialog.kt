package com.apolorossi.financask.ui.dialog

import android.content.Context
import android.view.ViewGroup
import com.apolorossi.financask.R
import com.apolorossi.financask.model.Type

class AddTransactionDialog(mViewGroup: ViewGroup,
                           mContext: Context) : TransactionDialog(mContext, mViewGroup) {
    override val positiveButtonTitle: String
        get() = "Adicionar"

    override fun titleByType(type: Type): Int {
        if (type == Type.RECEITA) {
            return R.string.adiciona_receita
        }
        return R.string.adiciona_despesa
    }
}
