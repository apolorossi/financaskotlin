package com.apolorossi.financask.ui

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import com.apolorossi.financask.R
import com.apolorossi.financask.extension.formatToBrazilianCurrency
import com.apolorossi.financask.model.Resume
import com.apolorossi.financask.model.Transaction
import kotlinx.android.synthetic.main.resumo_card.view.*
import java.math.BigDecimal

class ResumeView (context: Context,
                  private val view : View,
                  transactionsList: List<Transaction>) {

    private val resume: Resume = Resume(transactionsList)
    // Atalho CTRL + ALT + F: Extrai o que está selecionado para uma property
    private val revenueColor = ContextCompat.getColor(context, R.color.receita)
    private val expenseColor = ContextCompat.getColor(context, R.color.despesa)

    fun update(){
        addRevenue()
        addExpense()
        addTotal()
    }

    private fun addRevenue() {
        val totalRevenue = resume.revenue

        // If you put '!!' in front view, you are accepting the responsability that the view will not be null.

        // Garante que um valor que PODE SER NULO seja tratado da maneira adequada.
        /**
         *  The use of '.let' sends the view? to the scope
         *  of it's own method as non null.
         *
         *  The use of safe call --> '?' after view garantes that 'view' is not null, it is the same as 'if (view? != null)'
         *  but your attributes can be null then can be done encadiated safe calls
         */
        /*view?.let {
            with(it.resumo_card_receita) {
                setTextColor(revenueColor)
                text = totalRevenue.formatToBrazilianCurrency()
            }
        }*/

        with(view.resumo_card_receita) {
            setTextColor(revenueColor)
            text = totalRevenue.formatToBrazilianCurrency()
        }
    }

    private fun addExpense() {
       val totalExpenses = resume.expense

        with(view.resumo_card_despesa) {
            setTextColor(expenseColor)
            text = totalExpenses.formatToBrazilianCurrency()
        }
    }

    private fun addTotal() {
        val total = resume.total
        val color = colorByValue(total)

        with(view.resumo_card_total) {
            setTextColor(color)
            text = total.formatToBrazilianCurrency()
        }
    }

    fun colorByValue(total: BigDecimal): Int {
        if (total >= BigDecimal.ZERO) {
            return revenueColor
        }
            return expenseColor
    }


}