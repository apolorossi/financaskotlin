package com.apolorossi.financask.ui.dialog

import android.content.Context
import android.view.ViewGroup
import com.apolorossi.financask.R
import com.apolorossi.financask.extension.formatToBrazilian
import com.apolorossi.financask.model.Transaction
import com.apolorossi.financask.model.Type

class UpdateTransactionDialog(mViewGroup: ViewGroup,
                              private val mContext : Context) : TransactionDialog (mContext, mViewGroup) {
    override val positiveButtonTitle: String
        get() = "Alterar"

    fun show(transaction: Transaction,
             delegate: (transaction : Transaction) -> Unit) {
        super.setupTransationDialog(transaction.type, delegate)

        initializeFields(transaction)
    }

    fun initializeFields(transaction: Transaction) {
        initializeValueField(transaction)
        initializeDateField(transaction)
        initializeCategoryField(transaction)
    }

    fun initializeCategoryField(transaction: Transaction) {
        val returnedCategories = mContext.resources.getStringArray(categoryByType(transaction.type))
        categoryField.setSelection(returnedCategories.indexOf(transaction.category), true)
    }

    fun initializeDateField(transaction: Transaction) {
        dateField.setText(transaction.date.formatToBrazilian())
    }

    fun initializeValueField(transaction: Transaction) {
        valueField.setText(transaction.value.toString())
    }

    override fun titleByType(type: Type): Int {
        if (type == Type.RECEITA) {
            return R.string.altera_receita
        }
        return R.string.altera_despesa
    }

}
