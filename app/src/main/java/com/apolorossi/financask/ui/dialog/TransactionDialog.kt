package com.apolorossi.financask.ui.dialog

import android.app.DatePickerDialog
import android.content.Context
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.apolorossi.financask.R
import com.apolorossi.financask.extension.formatToBrazilian
import com.apolorossi.financask.extension.parseToCalendar
import com.apolorossi.financask.model.Transaction
import com.apolorossi.financask.model.Type
import kotlinx.android.synthetic.main.form_transacao.view.*
import java.math.BigDecimal
import java.util.*

abstract class TransactionDialog(private val mContext: Context,
                             private val mViewGroup: ViewGroup) {

    private val dialogView = createLayout()
    protected val valueField = dialogView.form_transacao_valor
    protected val dateField = dialogView.form_transacao_data
    protected val categoryField = dialogView.form_transacao_categoria

    protected abstract val positiveButtonTitle : String

    fun setupTransationDialog(type: Type,
                              delegate: (transaction : Transaction) -> Unit) {

        setupDataField()

        setupCategoryField(type)

        setupDialog(type, delegate)
    }

    private fun setupDialog(type: Type, delegate: (transaction: Transaction) -> Unit) {
        val title = titleByType(type)

        AlertDialog.Builder(mContext)
                .setTitle(title)
                .setView(dialogView)
                .setPositiveButton(positiveButtonTitle)
                { _, i ->

                    val valueInText = valueField.text.toString()
                    val dateInText = dateField.text.toString()
                    val category = categoryField.selectedItem.toString()

                    var value = convertValueField(valueInText)

                    val date = dateInText.parseToCalendar()

                    val createdTransaction = Transaction(type = type,
                            value = value,
                            date = date,
                            category = category)

                    delegate(createdTransaction)
                }
                .setNegativeButton("Cancelar", null)
                .show()
    }

    abstract fun titleByType(type: Type): Int

    private fun convertValueField(value: String): BigDecimal {
        return try {
            BigDecimal(value)
        } catch (exception: NumberFormatException) {
            Toast.makeText(mContext, "Falaha na conversão de valor",
                    Toast.LENGTH_LONG)
                    .show()
            BigDecimal.ZERO
        }
    }

    fun setupCategoryField(type: Type) {

        val category = categoryByType(type)

        val categoriesAdapater = ArrayAdapter
                .createFromResource(
                        mContext,
                        category,
                        android.R.layout.simple_spinner_dropdown_item)

        categoryField.adapter = categoriesAdapater
    }

    fun categoryByType(type: Type): Int {
        if (type == Type.RECEITA) {
            return R.array.categorias_de_receita
        }
        return R.array.categorias_de_despesa

    }

    fun setupDataField() {

        val date = Calendar.getInstance()

        val year = date.get(Calendar.YEAR)
        val month = date.get(Calendar.MONTH)
        val day = date.get(Calendar.DAY_OF_MONTH)


        dateField.setText(Calendar.getInstance().formatToBrazilian())

        dateField.setOnClickListener {
            DatePickerDialog(mContext,
                    { _, year, month, day ->

                        val selectedDate = Calendar.getInstance()

                        selectedDate.set(year, month, day)
                        dateField.setText(selectedDate.formatToBrazilian())

                    }, year, month, day)
                    .show()
        }
    }

    fun createLayout(): View {
        return LayoutInflater.from(
                mContext)
                .inflate(R.layout.form_transacao,
                        mViewGroup,
                        false)  // Serve pra indicar se vai add isso agora na view, porém só vai queremos attachar quando der o show no alertDialog.
    }
}