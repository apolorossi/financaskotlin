package com.apolorossi.financask.model

import java.math.BigDecimal

class Resume(private val transactionsList : List<Transaction>) {

    val revenue get() = sumBy(Type.RECEITA)

    val expense get() = sumBy(Type.DESPESA)

    val total get() = revenue.subtract(expense)

    private fun sumBy(type : Type) : BigDecimal{
        val sumTransactionsByType = BigDecimal(transactionsList
                .filter { it.type == type }
                .sumByDouble { it.value.toDouble() })
        return sumTransactionsByType
    }

}