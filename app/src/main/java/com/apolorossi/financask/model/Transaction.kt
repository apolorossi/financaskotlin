package com.apolorossi.financask.model

import java.math.BigDecimal
import java.util.*

class Transaction(val value: BigDecimal,
                  val category: String = "Indefinida",
                  val type : Type,
                  val date: Calendar = Calendar.getInstance())